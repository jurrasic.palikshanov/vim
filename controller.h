#ifndef VIM_CONTROLLER_H
#define VIM_CONTROLLER_H

#define _CRT_SECURE_NO_WARNINGS
#include "model.h"

#include <iostream>
#include <fstream>
#include <sstream>

class VimController
{
public:
    // Model
    VimModel Model;

    // Arguments
    void get_args(int argc, char** argv);

    // Constuctor
    VimController();

public:
    // Program exit flag
    bool exit = false;

    // File modified flag
    bool file_modified = false;

    // Common mode functions
    void arrows(int symbol);
    void pages(int symbol);

    // Navigation Mode
    void navigaion_edit_mode();
    void navigation_edit_long_command_check();
    void navigation_edit_enter();
    void get_num(int symbol);

    // Insert Mode
    void insert_mode();

    // Command mode
    void command_mode();
    void command_parcing();

    // Search mode
    bool search_get_string(int symbol);

    // File methods
    MyString cur_file; // Current file name
    void open_file(const char* file_name);
    void save_to_cur_file();
    void save_to_new_file(const char* file_name);
};

#endif