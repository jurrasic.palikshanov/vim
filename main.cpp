#include <iostream>
#include "controller.h"

int main(int argc, char** argv)
{	
	// [ User       ---> Controller ] 
	// [ Controller ---> Model      ]
	// [ Model      ---> View       ]
	VimController Controller;

	// Check if arguments are exist and get them
	Controller.get_args(argc, argv);

	// Start the main cycle
	Controller.navigaion_edit_mode();
	
    return 0;
}