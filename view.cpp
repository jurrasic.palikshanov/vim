#include "view.h"

// Constructor
VimView::VimView()
{
    // Initialization and translation to curses mode
    initscr();
    // Don't echo() while we do getch()
    noecho();
    // Turn on KEY macros mode and e.t.c.
    keypad(stdscr, true);
    // idk
    use_default_colors();
    // Init color
    design();

    // init booleans
    num_indent = false;
}

// Destructor
VimView::~VimView()
{
    // Off the color
    attroff(COLOR_PAIR(1));

    // Deallocates memory and ends curses mode
    endwin();
}

// Set the string design (view)
void VimView::design()
{
    start_color();			
	init_pair(1, COLOR_GREEN, -1);
    init_pair(2, COLOR_WHITE, -1);
    attron(COLOR_PAIR(1));
}

void VimView::delay(bool turn)
{
    if (turn)
        halfdelay(8);
    else
        cbreak();
}

// Update status bar
void VimView::status_bar(unsigned int cur_line, 
                        unsigned int cur_pos, 
                        unsigned int max_lines, 
                        unsigned int max_line_pos,
                        MyString command,
                        unsigned int show_line,
                        unsigned int cur_mode,
                        MyString tmp,
                        MyString tmp_2)
{
    // Output Information under text
    attron(COLOR_PAIR(2));
    if (cur_mode == 2) 
    {
        // Output command line
        if (command.size() > 0)
            mvprintw(max_y - 1, 0, "  command:%s", command.c_str());
        else
            mvprintw(max_y - 1, 0, "  command:");
        refresh();
    }
    else
    {
        // Navigation and edit information
        mvprintw(max_y - 1, 0, "%s               VIM-P v.1.0    cur %d,%d  max %d,%d  show %d  x,y %d,%d ",
            command.c_str(),
            cur_line,
            cur_pos,
            max_lines,
            max_line_pos,
            show_line,
            max_x, 
            max_y);
        
        // Current mode 
        switch (cur_mode)
        {
        case 0:
            addstr("[navigation]");
            break;    
        case 1:
            addstr("[insert]");
            break;
        case 3:
            addstr("[search]");
            break;
        default:
            break;
        }
    }
    
    attron(COLOR_PAIR(1));

    // Output horizont and set cursor to the start position
    output_horizont();
    move (0, 0);
}

// Output text indent 
void VimView::text_indent(int num)
{
    if (num_indent) // Num indent
    {
        mvprintw(num, 0,"%d", num + 1);
        if (num + 1 < 1000) addch(' ');
        if (num + 1 < 100) addch(' ');
        if (num + 1 < 10) addch(' ');
    }
    else // Default indent
    {
        addch('$'); 
        addch(' '); 
    }
}

// Output one symbol
void VimView::output_symbol(char symbol)
{
    addch(symbol);
}

// Cursor
void VimView::move_cursor(int line, int pos)
{
    move(line, pos);
}

// Update X and Y
void VimView::update_x_y()
{
    getmaxyx(stdscr, max_y, max_x);
}

// Make horizont
void VimView::output_horizont()
{
    int i = 1;
    mvprintw(max_y - 2, 0, "$");
    while (i < max_x - 1)
    {
        addch('-');
        i++;
    }
    addch('$');
}

// Output help 
void VimView::output_help()
{
    clear();

    mvprintw(0,0, "          ");
    mvprintw(1,0, " [Navigation and Edit mode] ");
    mvprintw(2,0, "   $ - Move cursor to end of line ");
    mvprintw(3,0, "   w - Move the cursor to the end of the word to the right of the cursor");
    mvprintw(4,0, "   b - Move the cursor to the beginning of a word to the left of the cursor");
    mvprintw(5,0, "   gg - Move to the beginning of the file");
    mvprintw(6,0, "   G - Move to the end of the file");
    mvprintw(7,0, "   NG - Move to line with number N");
    mvprintw(8,0, "   x - Delete character after cursor");
    mvprintw(9,0, "   diw - Delete the word under the cursor, including the space to the right");
    mvprintw(10,0, "  dd - Cut the current line");
    mvprintw(11,0, "  y - Copy current line");
    mvprintw(12,0, "  yw - Copy word under cursor");
    mvprintw(13,0, "  p - Paster after cursor");
    mvprintw(15,0, "          Press any buttom to continue ... ");
}
