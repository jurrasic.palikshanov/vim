#ifndef VIM_VIEW_H
#define VIM_VIEW_H

#include <ncurses.h>
#include <iostream>
#include "my_string.h"

class VimView
{
// Text size (updating in model)
public:
    unsigned int max_x;
    unsigned int max_y;
public:
    void update_x_y();

// Stutus bar
public:
    void status_bar(unsigned int cur_line, 
                    unsigned int cur_pos, 
                    unsigned int max_lines, 
                    unsigned int max_line_pos,
                    MyString command,
                    unsigned int show_line,
                    unsigned int cur_mode,
                    MyString tmp,
                    MyString tmp_2);

// Print text 
public:
    void output_symbol(char symbol);
    void output_string();
    void text_indent(int num);
    bool num_indent; // Changing by controller (must be public)
    void output_help();

// Options
public:
    void design();
    void delay(bool turn);

// Cursor
public:
    void move_cursor(int line, int pos);


// Command line
public:
    void output_command_line(MyString command);

// Horizont
public:
    void output_horizont();

// Constructor and destructor
public:
    VimView();
    ~VimView();

};

#endif

// Breakpoint

//mvprintw(10, 10, "%d", index);
//  refresh();
//    while (true){ }