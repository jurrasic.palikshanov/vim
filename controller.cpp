#include "controller.h"

VimController::VimController()
{

}

// Hanldle arrow keys
void VimController::arrows(int symbol)
{
    switch (symbol)
    {
    case KEY_LEFT: 
            Model.left();
            break;
        case KEY_RIGHT:
            Model.right();
            break;
        case KEY_DOWN:
            Model.down();
            break;
        case KEY_UP:
            Model.up();
            break;
    default:
        break;
    }
}

// Handle page up and page down keys
void VimController::pages(int symbol)
{
    switch (symbol)
    {
    case KEY_PPAGE:
        Model.page_up();
        break;
    case KEY_NPAGE:
        Model.page_down();
        break;
    default:
        break;
    }
}

// Navigaion mode (First mode)
void VimController::navigaion_edit_mode() 
{
    // Getiing symbols
    int symbol;

	while (true)
	{
		symbol = getch();

        switch (symbol)
        {
        case 'i':
            insert_mode(); break;
        case 'I':
            Model.cursor_start_of_string();
            insert_mode(); break;
        case 'A':
            Model.cursor_end_of_string();
            insert_mode(); break;
        case 'S':
            Model.del_line();
            insert_mode(); break;
        case '$':
            Model.cursor_end_of_string(); break;
        case 'w':
            Model.cursor_end_of_next_word(); break;
        case 'G':
            Model.cursor_file_end(); break;
        case 'b':
            Model.cursor_start_of_last_word(); break;
        case '0': case '^':
            Model.cursor_start_of_string(); break;
        case 'x':
            Model.delete_after_cursor(); break;
        case 'p':
            Model.paste(); file_modified = true; break;
        case 'r':
            symbol = getch();
            Model.symbol_change(symbol); file_modified = true; break;
        case ':':
            command_mode(); break;
        case '/':
            if(search_get_string(symbol))
                Model.search_end();
            break; 
        case '?':
            if(search_get_string(symbol))
                Model.search_start();
            break;
        case 'n':
            if (Model.get_search_dir())
                Model.search_start();
            else
                Model.search_end();
            break;
        case 'N':
            if (Model.get_search_dir())
                Model.search_end();
            else
                Model.search_start();
            break;

        case ERR:
            break;

        case KEY_UP: case KEY_DOWN: case KEY_RIGHT: case KEY_LEFT:
            arrows(symbol); break;
        case KEY_PPAGE: case KEY_NPAGE:    
            pages(symbol); break;

        case '1': case '2': case '3': case '4': case '5': 
        case '6': case '7': case '8': case '9':
            get_num(symbol);
            break;

        // Letters used in long commands 
        // Functions returns false if delete, 
        // true if continue cycle without deleting
        case 'd': case 'y': case 'g':
            Model.command.append(1, symbol);
            navigation_edit_long_command_check(); 
        default:
            break;
        }

        // Clear (crutch, but it works great)
        // Model.command.clear();
        while (Model.command.size() > 0) 
            Model.command.erase(0, 1);

        // Output changes
        Model.output_stdscr();

        // Check for exit
        if (exit) 
            return;
	}
}

// Check letters of long commands like (diw, dd, yw)
void VimController::navigation_edit_long_command_check()
{
    int symbol;

    while (Model.command.size() < 4)
    {
        // Check for dd, gg, yw, diw (contunue adding this buffer if first symbol is 'd')
        if (Model.command.size() == 1 && (Model.command[0] != 'd'
        && Model.command[0] != 'y' &&  Model.command[0] != 'g'))
            break;

        // Check for dd, if its true then call function
        if (Model.command.size() == 2 && Model.command[0] == 'd' && Model.command[1] == 'd')
        {
            Model.cut(); break;
        }

        // Check for g, if its true then call function
        if (Model.command.size() == 2 && Model.command[0] == 'g' && Model.command[1] == 'g')
        {
            Model.cursor_file_start(); break;
        }

                // Check for yw, if its true then call cur function
        if (Model.command.size() == 2 && Model.command[0] == 'y' && Model.command[1] == 'w')
        {
            Model.copy_word(); break;
        }

        // Check for diw (contunue adding this buffer if first symbol is 'd' and second one is 'i')
        if (Model.command.size() == 2 && Model.command[0] != 'd' && Model.command[1] != 'i') 
            break;

        // Check for diw, if its true then call cur function
        if (Model.command.size() == 3 && Model.command[0] == 'd' && Model.command[1] == 'i' && Model.command[2] == 'w')
        {
            Model.delete_word_and_spaces(); break;
        }

        // If it Esc or backspace then stop input long command
        if (Model.command[Model.command.size() - 1] == 27 || 
            Model.command[Model.command.size() - 1] == KEY_BACKSPACE)
            break;

        // Output part of command
        Model.output_stdscr();

        // Add new symbol
        symbol = getch();
        Model.command.append(1, symbol);

        // Enter - find and execute inputed command 
        if (symbol == 10) 
        {
            navigation_edit_enter(); break;
        }
    }

    // Clear buffer after all
    while (!Model.command.empty()) 
        Model.command.erase(0, 1);

    return;
}

// Execute command from command_buffer
void VimController::navigation_edit_enter()
{
    if (Model.command.size() == 2 && Model.command[0] == 'y')
        Model.copy();
}

// Insert mode 
void VimController::insert_mode()
{
    // Output screen after mode switch
    Model.cur_mode = 1;
    Model.output_stdscr();

    // Getiing symbols
    int symbol;
	while (true)
	{
		symbol = getch();
       switch (symbol)
       {
       case KEY_LEFT: 
           Model.left(); break;
       case KEY_RIGHT:
           Model.right(); break;
       case KEY_DOWN:
           Model.down(); break;
       case KEY_UP:
           Model.up(); break;
       case KEY_PPAGE:
           Model.page_up(); break;
       case KEY_NPAGE:
           Model.page_down(); break;
       case KEY_BACKSPACE:
           Model.backspace(); break;
       case 10: // KEY_ENTER
           Model.enter(); file_modified = true; break;
       case '\t': // Tab key
           Model.tab();
       case ERR:
           break;
       case 27: // KEY ESC
           // Output screen after mode switch
           Model.output_stdscr();
           Model.cur_mode = 0;
           return;
       default:
           Model.insert(symbol); file_modified = true; break;
       }
       Model.output_stdscr();
	}
}

// Command input mode
void VimController::command_mode()
{ 
    // Init
    while (!Model.command.empty()) // Clear (crutch, but it works great)
        Model.command.erase(0, 1);
    Model.cur_mode = 2;

    // Getting command
    bool stop = false;
    while (true)
    {
        Model.output_stdscr();

        int symbol = getch();

        switch (symbol)
        {
        case 10: // Enter
            command_parcing();
            stop = true;
            break;
        case KEY_BACKSPACE:
            if (Model.command.size() != 0)
                Model.command.erase(Model.command.size() - 1, 1);
            else
                stop = true;
            break;
        case 27: // ESC
            stop = true;
        case ERR:
            break;
        default:
            Model.command.append(1, symbol);
            break;
        } 

        if (stop) 
            break; 
    }

    // Deinit
    while (!Model.command.empty()) // Clear (crutch, but it works great)
        Model.command.erase(0, 1);

    Model.cur_mode = 0;
    Model.View.move_cursor (Model.cur_line, Model.cur_pos + Model.start_pos); 
}

// Parcing command 
void VimController::command_parcing()
{
    // Switch indent type (num or just symbol)
    if (my_strcmp(Model.command.c_str(), "set num") == 0)
    { 
        if (!Model.View.num_indent)
        {
            Model.View.num_indent = true;
            Model.start_pos = 4;
        }
        else
        {
            Model.View.num_indent = false;
            Model.start_pos = 2;
        }
    }

    // Quit without saving
    if (my_strcmp(Model.command.c_str(), "q!") == 0)
        exit = true;

    // Quit if file has not been changed
    if (my_strcmp(Model.command.c_str(), "q") == 0)
        if (!file_modified)
            exit = true;

    // Output help
    if (my_strcmp(Model.command.c_str(), "h") == 0)
    {
        Model.View.output_help();

        // Press any buttom to contunue
        getch();
    }

    // Open file
    if (Model.command[0] == 'o')
    {
        // Delete 'o' & close current file
        Model.command.erase(0, 2); 
        Model.editor_start_position();
        open_file(Model.command.c_str());
    }

    // Save to current file and exit
    if (Model.command[0] == 'x'
    || my_strcmp(Model.command.c_str(), "wq!") == 0)
    {
        save_to_cur_file();
        exit = true;
    }

    // Save file
    if (Model.command[0] == 'w')
    {
        if (Model.command.size() > 1)
        {
            // Delete 'w' & and call saving
            Model.command.erase(0, 2);
            save_to_new_file(Model.command.c_str());
        }
        else
            save_to_cur_file();
    }

    // Check for number, then go to the chosen line
    if (Model.command[0] < 58 && Model.command[0] > 47)
    {
        // Translate command to number
        int i = Model.command.size() - 1, step = 1;
        int new_line = 0;

        for(; i >= 0; i--, step *= 10)
            new_line += (Model.command[i] - 48) * step;

        // Go to the new line
        if (new_line <= Model.max_lines)
        {
            Model.cur_line = new_line;
            Model.show_line = new_line;
        }
    }
}

// Getting string for search
bool VimController::search_get_string(int symbol)
{
    // Init
    while (Model.buffer_search.size() > 0) // Clear (crutch, but it works great)
        Model.buffer_search.erase(0, 1);

    Model.command.append(1, symbol);
    Model.cur_mode = 3;
    
    int symbol_getch;
    while (true)
    {
        Model.output_stdscr();
        symbol_getch = getch();
        
        if (symbol_getch == 10 || symbol_getch == 27) // Enter and ESC
            break;
        else if (symbol_getch == KEY_BACKSPACE)
            Model.command.erase(Model.command.size() - 1, 1);
        else
            Model.command.append(1, symbol_getch);
    }

    // Deinit
    Model.cur_mode = 0;

    // Exit (ESC)
    if (symbol_getch == 27)
    {
        return 0;
    } 
    else // Enter
    {
        // Copy the search string and delete first symbol
        Model.buffer_search.insert(0, Model.command.c_str());   
        Model.buffer_search.erase(0, 1);

        // Get direction
        Model.search_dir = (symbol == '?') ? true : false;
        return 1;
    }
}

// Arguments and init window
void VimController::get_args(int argc, char** argv)
{
    if (argc < 2)
        return;

    open_file(argv[1]);
}

// Open file
void VimController::open_file(const char* file_name)
{
    ifstream f;
    f.open(file_name);

    if (!f.is_open())
        return;

    char symbol;
 
    while(!f.eof())
    {
        f.get(symbol);
    
        if (symbol == '\n')
            Model.enter();
        else
            Model.insert(symbol);
    }

    // Update screen after adding file text
    Model.cursor_file_end(); 
    Model.output_stdscr();

    // Save file name
    cur_file = file_name;
}

// Save to current file current buffers
void VimController::save_to_cur_file()
{
    ofstream f;
    f.open(cur_file.c_str(), ios_base::out | ios_base::trunc);
    f << Model.buffer_before.c_str();
    f << Model.buffer_after.c_str();
}

// Save to new file
void VimController::save_to_new_file(const char* file_name)
{
    ofstream f;
    f.open(file_name, ios_base::out | ios_base::trunc);

    if (!f.is_open())
        return;

    f << Model.buffer_before.c_str();
    f << Model.buffer_after.c_str();
}

// Go to a specific line
void VimController::get_num(int symbol)
{
    // Save first symbol and output
    Model.command.append(1, symbol);
    Model.output_stdscr();

    while (true)
    {
        int symbol_getch = getch();

        if (symbol_getch == 'G')
        {
            Model.go_to_specific_line();
            break;
        }
        else
        {
            // If symbol is not number
            if (symbol_getch > 57 || symbol_getch < 48) 
                break;

            Model.command.append(1, symbol_getch);
        }
        
        // Output part of command
        Model.output_stdscr();
    }

    // Clear buffer after all
    while (!Model.command.empty()) 
        Model.command.erase(0, 1);
}

