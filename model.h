#ifndef VIM_MODEL_H
#define VIM_MODEL_H

#include "my_string.h"
#include "view.h"
#include <vector>

using namespace std;

class VimModel
{
    // class View
public:
    VimView View;
    
    // Current cursor position
    unsigned int cur_line;
    unsigned int cur_pos;

    // Position main text is starting
    unsigned int start_pos;

    // Count the all symbols in every line
    vector<unsigned int> lines;
    unsigned int max_lines;

    // Buffers
    MyString buffer_before = "";
    MyString buffer_after = "";
    MyString command = "";

    // 0 - NavigationEdit
    // 1 - Insert
    // 2 - Command mode
    // 3 - Search mode
    int cur_mode;

    // Constructor
    VimModel();
    void insert(int symbol);
    void backspace();
    void left();
    bool right();
    void enter();
    void up();
    void down();
    void tab();
    void page_up();
    void page_down();
    void del_line();

    // Find buffer index by cursor position 
    // Or find cursor by index 
    unsigned int get_index();
    void get_cur_after(int index);
    void get_cur(int index);

    // Swapping
    void swapping();

    // Split by enter or changing stucture by backspace
    bool check_split_enter();
    void recount_lines();

    // Command parcing
    bool command_parcing_two_symbols();

    // Command mode methods
    void cursor_end_of_string();
    void cursor_start_of_string();
    void cursor_start_of_last_word();
    void cursor_end_of_next_word();
    void cursor_file_end();
    void delete_after_cursor();
    void cursor_file_start();
    void symbol_change(int symbol);
    void go_to_specific_line();

    // Command mode methods with string copying
    MyString tmp = "";
    void cut();
    void copy();
    void paste();
    void delete_word_and_spaces();
    void copy_word();   

    // Call View
    void output_stdscr();
    void output_buffer();   

    // Line console showing text from
    unsigned int show_line; 
    void count_show();

    // Search 
public:
    MyString buffer_search = "";
    bool search_dir; // True - end, false - start
    void search_end();
    void search_start();
    bool get_search_dir();

    // Start program 
    void editor_start_position();
};

#endif