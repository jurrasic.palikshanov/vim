#include "model.h"

// Constructor
VimModel::VimModel()
{
    // Current cursor position data + cur mode
    cur_line = cur_pos = cur_mode = 0;

    // Text start position
    start_pos = 2;
    max_lines = 0;

    // Set size of lines vector
    lines.resize(1000);

    // Show line
    show_line = 0;

    // View part first call (needs)
    output_stdscr();
}

// Insert the new symbol from qetch
void VimModel::insert(int symbol)  
{  
    // Count pos and swap before append 
    swapping(); 
    buffer_before.append(1, symbol);

    // Step forward
    cur_pos++;
    lines[cur_line]++;
}  

// Delete the last symbol from first buffer
void VimModel::backspace()  
{ 
    // Check (1) if the buffer is empty
    if (buffer_before.size() == 0 && buffer_after.size() == 0)
        return;

    // Check (2) if cur position is first position
    if (cur_pos == 0 && cur_line == 0)
        return;

    // Count pos and swap before append 
    swapping();
    buffer_before.erase(buffer_before.size() - 1, 1); 

    if (cur_pos != 0)
    {
        // Step back
        cur_pos--;
        lines[cur_line]--;
    }
    else
    {
        // Step up, to the end of highest string 
        cur_line--;
        cur_pos = lines[cur_line] - 1;

        // Recount lines
        recount_lines();

        // Decrease max lines
        max_lines--;

        count_show();
    }
}   

// Move cursor left
void VimModel::left()  
{ 
    // Check (1)
    if (cur_pos == 0 && cur_line == 0)
        return;
        
    // Check (2)
    if (cur_pos == 0)
    {
        cur_line--;
        cur_pos = lines[cur_line] - 1;
        count_show();
    }
    else
        // Step back
        cur_pos--;
}

// Move cursor right
bool VimModel::right()
{
    // Check (1)
    if (lines[cur_line] <= cur_pos)
        return 1;

    // Check (2) dont move whan if '\n' or it max line
    if (max_lines != cur_line && lines[cur_line] == cur_pos + 1)
        return 1;

    // Step forward
    cur_pos++;
    return 0;
}

// Move tab
void VimModel::tab()  
{
    // Check
    if (lines[cur_line] <= cur_pos)
        return;

    // Step forward
    cur_pos++;
}

// Realize the enter
void VimModel::enter()  
{
    // Count pos and swap before append
    swapping(); 
    buffer_before.append(1, '\n');

    // Step on next line
    cur_pos = 0;
    lines[cur_line]++;
    cur_line++;
    max_lines++;

    // Check if enter has split the line
    if (check_split_enter())
        recount_lines();

    count_show();
}

// Move cursor up
void VimModel::up()  
{ 
    // Checking (1) 
    if (cur_line == 0)
        return;

    // Checking (2) 
    if (lines[cur_line - 1] > cur_pos)
    {
        // If higher string is bigger
        cur_line--;
    }
    else 
    {
        // If higher string is smaller or equal
        cur_line--;
        cur_pos = lines[cur_line] - 1;
    }

    count_show();
}

// Move cursor down (!)
void VimModel::down()  
{ 
    // Checking (1)  lower sting is emtpy
    if (cur_line == max_lines)
        return;

    // Checking (3) other actions
    if (lines[cur_line + 1] > cur_pos)
    {
        // If lower string is bigger
        cur_line++;
    }
    else 
    {
        // If lower string is smaller or equal
        cur_line++;

        // If lower string is empty but, after '\n'
        if (lines[cur_line] == 0)
            cur_pos = 0;
        else
            cur_pos = lines[cur_line] - 1;

        // Try to go right
        if (lines[cur_line ] == 0) 
            right();
    }

    count_show();
}

// Find out the needed buffer index
unsigned int VimModel::get_index()
{
    int count = 0;

    // Going through ever line's quantity of symbols and sum it
    for (int i = 0; i < cur_line; i++)
        count += lines[i];

    // Dont forget about position in current line
    count += cur_pos;
    return count;
}

// (must be a long description)
void VimModel::swapping()
{
    // Count new len (size) for first buffer
    int index = get_index();

    // Swapping if new position is BIGGER than old (go rigth)
    while (buffer_before.size() < index)
    {
        // Save symbol
        char tmp = buffer_after[0];
    
        // Delete symbol from second buffer
        buffer_after.erase(0, 1); 
    
        // Append new symbol to the first buffer
        buffer_before.append(1, tmp);
    }

    // Swapping if new position is SMALLER than old (go left)
    while (buffer_before.size() > index)
    {
        // Save symbol
        char tmp = buffer_before[buffer_before.size() - 1];
    
        // Delete symbol from first buffer
        buffer_before.erase(buffer_before.size() - 1, 1); 
    
        // Insert new symbol to the start of second buffer
        buffer_after.insert(0, 1, tmp);
    }

    // If it equal, then do nothing
}

// Check if enter divide the string
bool VimModel::check_split_enter()
{
    // Check if second buffer is empty
    if (buffer_after.size() != 0) 
        return true;
    else
        return false;
}

// Walking through vector and count symbol and every line
void VimModel::recount_lines()
{
    int count = 0, i = 0, line_tmp = 0;

    // Empty the informaion about first lein
    lines[0] = 0;

    // Cycle for updating first buffer lines information
    while (buffer_before[i] != '\0')
    {
        lines[line_tmp]++;
        if (buffer_before[i] == '\n')
        {
            line_tmp++;
            lines[line_tmp] = 0;
        }
        i++;
    }

    // Cycle for updating second buffer lines information
    i = 0;
    while (buffer_after[i] != '\0')
    {
        lines[line_tmp]++;

        if (buffer_after[i] == '\n')
        {
            line_tmp++;
            lines[line_tmp] = 0;
        }
        i++;
    }

    // In order not to nullify all lines, 
    // just in case, update one after the last
    lines[line_tmp + 1] = 0;
}

// Set the cursor at the end of the current string (line)
void VimModel::cursor_end_of_string()  
{ 
    if (lines[cur_line] > 0)
        cur_pos = lines[cur_line] - 1;

    // Try to go right
    right();
}

// Set the cursor at the end of next word 
void VimModel::cursor_end_of_next_word()  
{
    // Check if cursor is on the last position of line
    if (cur_pos == lines[cur_line])
        return;

    unsigned int index = 0;
    swapping();

    // First cycle - pass the word
    do
    {
        right(); index++;
    } while (buffer_after[index] != ' '
        && buffer_after[index] != '\0'
        && buffer_after[index] != '\n');

    // Second cycle - pass extra spaces
    while (buffer_after[index] == ' ')
    {
        right(); index++;
    }
}

// Set the cursor at the start of current string (line)
void VimModel::cursor_start_of_string()
{
    cur_pos = 0;
}

// Set the cursor at the start of last word (?)
void VimModel::cursor_start_of_last_word()
{
    // Check if cursor is on the last position already or line empty
    if (cur_pos == 0)
        return;

    unsigned int index = get_index();
    swapping();

    do
    {
        cur_pos--; index--;
    } while (buffer_before[index] != ' '
        && cur_pos != 0); 

    // Second cycle - pass extra spaces
    while (buffer_before[index] == ' ' && cur_pos != 0)
    {
        cur_pos--; index--;
    }
}

// Set the cursor at the end of the file
void VimModel::cursor_file_end()
{
    cur_line = max_lines;
    cur_pos = lines[cur_line];
    if (cur_line > View.max_y - 3 + show_line)
        show_line = max_lines - View.max_y + 3;
}

// Delete symbol after cursor
void VimModel::delete_after_cursor()
{
    if (!right())
        backspace();
}

// Go to file first position
void VimModel::cursor_file_start()
{
    cur_pos = 0;
    cur_line = 0;
    show_line = 0;
}

// Output the strings and other 
void VimModel::output_stdscr()
{
    // Clear the screen (stdscr)
    clear();

    // Update Max X and Max Y coordinates
    View.update_x_y();

    // Status bar update
    View.status_bar(cur_line, 
                cur_pos, 
                max_lines,
                lines[cur_line],
                command,
                show_line, 
                cur_mode, 
                tmp,
                buffer_after);
    

    // Save the size and append second buffer to the first
    size_t first_buffer_size = buffer_before.size();
    buffer_before.append(buffer_after.c_str());

    // Output Buffers
    output_buffer();

    // Erase second buffer from first
    buffer_before.erase(first_buffer_size, buffer_after.size());

    // Move 
    View.move_cursor(cur_line - show_line, cur_pos + start_pos);    

    // Update screen (stdscr)
    refresh();
}

// Output buffer (calling by output_stdscr)
void VimModel::output_buffer()
{
    // Data (iterator for buffer and current line tmp)
    int i = 0, line_num = 0, y = 0;

    // Skip part of the buffer that will not be shown
    while (buffer_before[i] != '\0' && show_line != line_num) 
    {
        if (buffer_before[i] == '\n')
            line_num++;
        i++;
    }  

    View.text_indent(0);  // First indent output

    while (buffer_before[i] != '\0' && y < View.max_y - 2) 
    {  
        if (buffer_before[i] == '\n')
        {
            // Output indent (new line)
            line_num++;
            View.output_symbol('\n');
            View.text_indent(line_num);
            y++;
        }
        else
            // Output symbol
            View.output_symbol(buffer_before[i]);
        i++;
    }
}

// Cut the current line
bool VimModel::command_parcing_two_symbols()
{
    // File first position
    if (command[0] == 'g' && command[1] == 'g')
    {
        cursor_file_start(); return 0;
    }

    // File first position
    if (command[0] == 'd' && command[1] == 'd')
    {
        cut(); return 0;
    }

    return 1;
}

// Find from which line the display will be
void VimModel::count_show()
{   
    // Going down
    if (cur_line >= show_line + View.max_y - 3)
        show_line = cur_line - View.max_y + 3;
    else 
        // Going up with decrease show_line
        if (cur_line < show_line)
            show_line = cur_line;
}

// Page up realization
void VimModel::page_up()
{
    int page_size = View.max_y - 2;

    // Decrease show line
    if (show_line > page_size)
        show_line -= page_size;
    else
        show_line = 0;

    // Decrease current line
    if (cur_line > page_size)
        cur_line -= page_size;
    else
        cur_line = 0;

    // Check current position (maybe change)
    cur_pos = 0;
}

// Page down realization
void VimModel::page_down()
{
    int page_size = View.max_y - 2;

    // Increase cur line
    if (max_lines >= page_size + cur_line)
        cur_line += page_size;
    else
        cur_line = max_lines;
    
    // Increase show line
    if (show_line + page_size <= max_lines - page_size + 1)
        show_line += page_size;
    else
        show_line = max_lines - page_size + 1;

    // Check current position (maybe change)
    cur_pos = 0;
}

// Cut the line (save to tmp buffer, and delete in text)
void VimModel::cut()
{
    copy();
    del_line();

    // Last backspace
    backspace();
}

// Copy the line (save to tmp buffer)
void VimModel::copy()
{
    // Clear (crutch, but it works great)
    while (!tmp.empty())
        tmp.erase(0, 1);

    // Save cur position and set line start (for get_index)
    int saved_pos = cur_pos;
    cursor_start_of_string();

    int index = get_index(), i = 0; 
    while (i < lines[cur_line])
    {
        tmp.append(1, buffer_before[index + i]);
        ++i; 
    }

    // Return saved position
    cur_pos = saved_pos;
}

// Delete the line
void VimModel::del_line()
{
    // Delete
    cursor_end_of_string();

    while (lines[cur_line] > 0 && cur_pos != 0)
        backspace();
}

// Input copied string (tmp)
void VimModel::paste()
{
    if (tmp.empty())
        return;

    int i = 0;
    while (i < tmp.size())
    {
        if (tmp[i] != '\n')
            insert(tmp[i]);
        else
            enter();
        ++i;
    }
}

// Set position (buffer_before index)
void VimModel::get_cur(int index)
{
    cur_pos = 0; cur_line = 0;

    for (int i = 0; i < index;)
        if (index > lines[cur_line] + i)
        {
            i += lines[cur_line];
            cur_line++;
        }
        else
        {
            cur_pos++;
            i++;
        }
}

// Set position (buffer_after index)
void VimModel::get_cur_after(int index)
{
    for (int i = 0; i < index; i++)
    {
        if (buffer_after[i] == '\n')
        {
            cur_line++; cur_pos = 0;
        }
        else
            cur_pos++;
    }
}

// Return the start position of variables of model part
void VimModel::editor_start_position()
{
    // Clear buffers
    if (buffer_before.size() > 0)
        buffer_before.erase(0, buffer_before.size());
    if (buffer_after.size() > 0)
        buffer_after.erase(0, buffer_after.size());

    cur_pos = 0;
    cur_line = 0;
    max_lines = 0;
    lines[cur_line] = 0;
}

// Change the symbol the cursor is on (bad optimization)
void VimModel::symbol_change(int symbol)
{
    right();
    backspace();
    insert(symbol);
    left();
}

// Copy word under cursor
void VimModel::copy_word()
{
    // Start options
    while (!tmp.empty()) 
        tmp.erase(0, 1);

    swapping();
    int index = get_index();

    // Find start of file (meet the space)
    while (buffer_before[index - 1] != ' ' && cur_pos > 1)
    {
        index--; left();
    }
      
    // Now cursor is on start of word 
    // Swapping and get all word
    swapping();

    index = 0;
    while(buffer_after[index] != ' ' && cur_pos < lines[cur_line])
    {
        tmp.append(1, buffer_after[index]);
        index++;
        right();
    }
}

// Delte word under cursor and right space
void VimModel::delete_word_and_spaces()
{
    // Start options
    while (!tmp.empty()) 
        tmp.erase(0, 1);

    swapping();
    int index = 0;

    // Find end of file (meet the space or line end)
    while(buffer_after[index] != ' ' && 
        buffer_after[index] != '\n' && 
        buffer_after[index] != '\0')
    {
        index++; right();
    }

    // Try to go right (for delete space 
    // and check the end of line)
    if (buffer_after[index] == ' ')
    {
        index++; right();
    }
      
    // Now cursor is on start of word 
    // Swapping and get all word
    swapping();

    index = buffer_before.size() - 1;
    do 
    {
        backspace();
        index--;
    } while (buffer_before[index] != ' ' && cur_pos > 0);
}

// Translate command to the string 
// and jump on this number line
void VimModel::go_to_specific_line()
{
    // Translate buffer to number
    int number = 0, step = 1;
    for (int i = command.size() - 1; i >= 0; i--, step *= 10)
        number += (command[i] - 48) * step;

    // Check number
    if (number > max_lines) return;

    // Jump
    cur_line = number;
    count_show();
}

// Start search process from cursor to file end (model)
void VimModel::search_end()
{
    // Must be buffer refresh before searcing 
    swapping();

    // Searching (start from next symbol)
    unsigned int new_buf_index = buffer_after.find(buffer_search.c_str(), 1);

    if (new_buf_index == -1)
        return;

    // Go to new position
    get_cur_after(new_buf_index);
    count_show();
}

// Start search process from cursor to file start (model)
void VimModel::search_start()
{
    // Must be buffer refresh before searcing 
    swapping();

    int index = buffer_before.size() - 1, step = 0;
    if (index > 0) 
        index--; // Dont check from current position

    for (; index >= 0; index--)
    {
        step = 0;
        while ((buffer_before[index + step] == buffer_search[step])
            && buffer_before[index + step] != '\0')
        {
            // If search is complete succesful
            if (step == buffer_search.size() - 1)
            {
                // Set cursor at needed position
                get_cur(index);

                // Set current show line
                count_show();

                cur_mode = 0;
                return;
            }
            step++;
        }
    }
}

// Search direction setter
bool VimModel::get_search_dir()
{
    return search_dir;
}

